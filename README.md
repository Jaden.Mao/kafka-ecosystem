# kafka-ecosystem

## K8S Operators
[The list of awesome operators](https://github.com/operator-framework/awesome-operators)

- [helm-chart/kafka](https://github.com/helm/charts/tree/master/incubator/kafka#connecting-to-kafka-from-outside-kubernetes) Apache Kafka Helm Chart
- [krallistic/kafka-operator](https://github.com/krallistic/kafka-operator) A Kafka Operator for Kubernetes
- [strimzi/strimzi](https://github.com/strimzi/strimzi-kafka-operator)	Operator for running Kafka and Kafka Connect on Kubernetes and OpenShift
- [banzaicloud/kafka-operator](https://github.com/banzaicloud/kafka-operator)  An operator that manages Kafka on Kubernetes.
- [kudobuilder/operators/kafka](https://github.com/kudobuilder/operators/tree/master/repository/kafka)	An Apache Kafka Operator built on KUDO

## Kafka tools
- [kafkacat](https://github.com/edenhill/kafkacat): is a command line utility that you can use to test and debug Apache Kafka® deployments
  - consumer mode(-C)
  ```bash
  kafkacat -b localhost:9092 -t mysql_users
  or 
  kafkacat -b localhost:9092 -t mysql_users -C 
  {"uid":1,"name":"Cliff","locale":"en_US","address_city":"St Louis","elite":"P"}
  ```
  - producer mode(-P)
  ```bash
  kafkacat -b localhost:9092 -t new_topic -P
  ```

## Kafka Connect
- [Confluent Hub](https://www.confluent.io/hub/)
- [Debezium](https://debezium.io/documentation/reference/1.0/connectors/index.html)

### MongoDB Connector
[MongoDB Connector for Apache Kafka](https://www.confluent.io/hub/mongodb/kafka-connect-mongodb)

- Setup a MongoDB Replica Set

```bash
rs.initiate(
   {
      _id: "myReplSet",
      version: 1,
      members: [
         { _id: 0, host: "mongo1:27017" },
         { _id: 1, host: "mongo2:27017" },
         { _id: 2, host: "mongo3:27017" }
      ]
   }
)
```

- MongoSourceConnector

config:
```json
{
    "name":"mongodb-source", 
    "config": 
    {
      "connector.class": "com.mongodb.kafka.connect.MongoSourceConnector",
      "database": "test",
      "topic.prefix": "mongo",
      "tasks.max": "1",
      "connection.uri": "mongodb://mongo1:27017,mongo2:27017,mongo3:27017",
      "name": "MongoSourceConnector",
      "collection": "source",
      "value.converter": "io.confluent.connect.avro.AvroConverter",
      "value.converter.schema.registry.url": "http://kafka-schema-registry:8081",
      "key.converter": "io.confluent.connect.avro.AvroConverter",
      "key.converter.schema.registry.url": "http://kafka-schema-registry:8081"
    }
}
```

- MongoSinkConnector

config:
```json
{
    "name":"mongodb-sink", 
    "config": 
    {
        "connector.class": "com.mongodb.kafka.connect.MongoSinkConnector",
        "tasks.max": "1",
        "topics": "sink-topic",
        "max.num.retries": "3",
        "collection": "sink",
        "database": "test",
        "connection.uri": "mongodb://mongo1:27017,mongo2:27017,mongo3:27017",
        "retries.defer.timeout": "5000",
        "key.converter":"org.apache.kafka.connect.json.JsonConverter",
        "key.converter.schemas.enable":false,
        "value.converter":"org.apache.kafka.connect.json.JsonConverter",
        "value.converter.schemas.enable":false
    }
}
```

- kafka consume and produce cli

```bash
# list topics
kafka-topics  --list --zookeeper zoo1:2181 

# produce message to topic
kafka-console-producer --broker-list localhost:9092 \
    --topic sink-topic 
>{"name":"rich", "age": 30}

# consume messages from topic
kafka-console-consumer --bootstrap-server localhost:9092 \ 
    --topic test --from-beginning
```

## Kafka Monitoring

## Reference

- [Kafka topic naming conventions](https://riccomini.name/how-paint-bike-shed-kafka-topic-naming-conventions)