# Configuring a single-node multiple-broker cluster on CentOS7

## Installation services

- Kafka 2.4.1
https://www.apache.org/dyn/closer.cgi?path=/kafka/2.4.1/kafka_2.13-2.4.1.tgz

- Scala 2.13
https://downloads.lightbend.com/scala/2.13.1/scala-2.13.1.rpm

- Zookeeper 3.5.6
https://archive.apache.org/dist/zookeeper/zookeeper-3.5.6/apache-zookeeper-3.5.6-bin.tar.gz

- Kafdrop
https://github.com/obsidiandynamics/kafdrop/archive/master.zip

## Creating a User for Kafka
```bash
#  create a user called kafka with the useradd command:
sudo useradd kafka -m

# Set the password using passwd
sudo passwd kafka

# Add the kafka user to the wheel group
sudo usermod -aG wheel kafka

# Log into this account using su
su -l kafka
```

## Install OpenJDK 11
1. Update the package repository to ensure you download the latest software
```bash
sudo yum update
```
2. Then, install the Java Development Kit with the following command:
```bash
sudo yum install java-11-openjdk-devel
```
3. set Environment variable
```bash
vim ~/.bashrc
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.6.10-1.el7_7.x86_64
export PATH=$PATH:$JAVA_HOME/bin
```

## Install Scala 2.13 
```bash
wget https://downloads.lightbend.com/scala/2.13.1/scala-2.13.1.rpm

sudo yum install scala-2.13.1.rpm

scala -version
```

## Run Zookeeper

- config
```bash
cat config/zookeeper.properties
```

```properties
dataDir=/home/kafka/zookeeper/data
clientPort=2181
maxClientCnxns=0
admin.enableServer=false
```

- start zookeeper in the background 
```bash
./bin/zookeeper-server-start.sh -daemon config/zookeeper.properties
```
## Downloading and Extracting the Kafka Binaries
```bash
tar -xvf kafka_2.13-2.4.1.tgz
```

## Run Kafka broker

- server configuration
```properties

# server1 config/server1.properties
broker.id=1
port=9092
log.dirs=/home/kafka/data/kafka-logs-1
offsets.retention.minutes=10080
log.retention.hours=720

# server2 config/server2.properties
broker.id=2
port=9093
log.dirs=/home/kafka/data/kafka-logs-2
offsets.retention.minutes=10080
log.retention.hours=720

# server3 config/server3.properties
broker.id=3
port=9094
log.dirs=/home/kafka/data/kafka-logs-3
offsets.retention.minutes=10080
log.retention.hours=720
```

- start Kafka instance
```bash
./bin/kafka-server-start.sh -daemon config/server1.properties
./bin/kafka-server-start.sh -daemon config/server2.properties
./bin/kafka-server-start.sh -daemon config/server3.properties
```

## Testing the Installation
```bash
./bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 3 --partitions 3 --topic test

./bin/kafka-topics.sh --describe --zookeeper localhost:2181 --topic test

echo "Hello, World" | /home/kafka/kafka_2.13-2.4.1/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test > /dev/null

/home/kafka/kafka_2.13-2.4.1/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning
```

## Run Kafdrop

- build jar
```bash
mvn compile package -Dmaven.test.skip=true
```
- run jar
```bash
java -jar target/kafdrop-3.24.0.jar
```
- test
```bash
curl localhost:9000
```

## Run Zookeeper & Kafka as Systemd service


### creating zookeeper Systemd unit file
sudo vi /etc/systemd/system/zookeeper.service

```properties
[Unit]
Requires=network.target remote-fs.target
After=network.target remote-fs.target

[Service]
Type=simple
User=kafka
ExecStart=/home/kafka/kafka_2.13-2.4.1/bin/zookeeper-server-start.sh /home/kafka/kafka_2.13-2.4.1/config/zookeeper.properties
ExecStop=/home/kafka/kafka_2.13-2.4.1/bin/zookeeper-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
```

- To start and enable Zookeeper on server boot
```bash
sudo systemctl start zookeeper
sudo systemctl status zookeeper
sudo systemctl enable zookeeper
```

- create startup shell script
```bash
vim start-kafka-cluster.sh

#!/bin/sh

/home/kafka/kafka_2.13-2.4.1/bin/kafka-server-start.sh -daemon /home/kafka/kafka_2.13-2.4.1/config/server1.properties

/home/kafka/kafka_2.13-2.4.1/bin/kafka-server-start.sh  -daemon /home/kafka/kafka_2.13-2.4.1/config/server2.properties

/home/kafka/kafka_2.13-2.4.1/bin/kafka-server-start.sh  -daemon /home/kafka/kafka_2.13-2.4.1/config/server3.properties
```

### creating kafka Systemd unit file
sudo vi /etc/systemd/system/kafka.service

```bash
[Unit]
Requires=zookeeper.service
After=zookeeper.service

[Service]
Type=forking
User=kafka
ExecStart=/home/kafka/kafka_2.13-2.4.1/start-kafka-cluster.sh
ExecStop=/home/kafka/kafka_2.13-2.4.1/stop-kafka-cluster.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
```

- To start and enable Kakfa on server boot
```bash
sudo systemctl start kafka
sudo systemctl status kafka
sudo systemctl enable kafka
```
