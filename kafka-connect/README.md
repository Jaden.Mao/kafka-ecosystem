## Kafka Connect

### Kafka JDBC Connect
- for DB2

```sql
CREATE TABLE logs(
    log_id INT GENERATED ALWAYS AS IDENTITY NOT NULL,
    message VARCHAR(255) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(log_id)
);

INSERT INTO TEST.LOGS (MESSAGE, CREATED_AT) VALUES('test', CURRENT TIMESTAMP);
```

```properties
connector.class=io.confluent.connect.jdbc.JdbcSourceConnector
mode=timestamp+incrementing
incrementing.column.name=LOG_ID
timestamp.column.name=CREATED_AT
topic.prefix=db2-test-
connection.password=87654321
tasks.max=1
connection.user=db2inst1
connection.url=jdbc:db2://3.113.16.61:50000/testdb
table.whitelist=TEST.LOGS
```

```bash
curl -X POST http://localhost:8083/connectors -H "Content-Type: application/json" -d '{
      "name": "db2_jdbc_source_log",
      "config": 
      {
  "connector.class": "io.confluent.connect.jdbc.JdbcSourceConnector",
  "mode": "timestamp+incrementing",
  "timestamp.column.name": "CREATED_AT",
  "incrementing.column.name":"LOG_ID",
  "topic.prefix": "db2-jdbc-connect-",
  "connection.password": "87654321",
  "tasks.max": "1",
  "connection.user": "db2inst1",
  "connection.url": "jdbc:db2://3.113.16.61:50000/testdb",
  "table.whitelist": "TEST.LOGS"
}
}'
```