## Change Data Capture (CDC) with Debezium

### Debezium introduction
- Debezium is a distributed platform built for CDC. 
- It uses database transaction logs and creates event streams on row-level changes. 
- Debezium provides a library of [connectors](https://debezium.io/documentation/reference/0.10/connectors/index.html), supporting a variety of databases available today. 
  - These connectors can monitor and record row-level changes in the schemas of a database. 
  - They then publish the changes on to a streaming service like Kafka.

- Normally, one or more connectors are deployed into a Kafka Connect cluster and are configured to monitor databases and publish data-change events to Kafka. 
- A distributed Kafka Connect cluster provides the fault tolerance and scalability needed, ensuring that all the configured connectors are always running.

---

### Build local env.

- Start all services
```bash
docker-compose up -d
```

- Check service status
```bash
docker-compose ps
```

- Get into MongoDB container

```bash
docker-compose exec mongo1 bash
```

- Initialize Mongodb replica set

![mongodb-replica-set](./img/replica-set.png)

```js
rs.initiate(
  {
    _id : 'rs0',
    members: [
      { _id : 0, host : "mongo1:27017" },
      { _id : 1, host : "mongo2:27017" },
      { _id : 2, host : "mongo3:27017" }
    ]
  }
);

// check the status of the replica set  
rs.status();
```

- MongoDB source connector configuration

```properties
connector.class=io.debezium.connector.mongodb.MongoDbConnector
tasks.max=1
mongodb.name=LocalReplica
mongodb.hosts=rs0/mongo1:27017
collection.whitelist=test.inventory,test.user
```

- MySQL source connector configuration
```properties
name=inventory-connector
connector.class=io.debezium.connector.mysql.MySqlConnector
tasks.max=1
database.hostname=mysql
database.port=3306
database.user=debezium
database.password=dbz
database.server.id=184054
database.server.name=dbserver1
database.whitelist=inventory
database.history.kafka.bootstrap.servers=broker:29092
database.history.kafka.topic=dbhistory.inventory
```

- Insert data to MongoDB (inventory)
```js
db.inventory.insertMany( [
   { item: "notebook", qty: 50, size: { h: 8.5, w: 11, uom: "in" }, status: "P" },
   { item: "paper", qty: 100, size: { h: 8.5, w: 11, uom: "in" }, status: "D" },
   { item: "planner", qty: 75, size: { h: 22.85, w: 30, uom: "cm" }, status: "D" },
   { item: "postcard", qty: 45, size: { h: 10, w: 15.25, uom: "cm" }, status: "A" },
   { item: "sketchbook", qty: 80, size: { h: 14, w: 21, uom: "cm" }, status: "A" },
   { item: "sketch pad", qty: 95, size: { h: 22.85, w: 30.5, uom: "cm" }, status: "A" }
] );
```

- Update document in inventory (item is paper)
```js
db.inventory.updateOne(
   { item: "paper" },
   {
     $set: { "size.uom": "cm", status: "P" },
     $currentDate: { lastModified: true }
   }
)
```