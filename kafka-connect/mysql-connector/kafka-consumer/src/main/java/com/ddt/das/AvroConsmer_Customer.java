/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ddt.das;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.record.TimestampType;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

/**
 * @author rich
 *
 * Read data from Kafka with Schema-registry
 */
public class AvroConsmer_Customer {
    private static String KAFKA_BROKER_URL    = "localhost:9092";
    private static String SCHEMA_REGISTRY_URL = "http://localhost:8081";
    private static String CONSUMER_GROUP      = "group1";

    private static ObjectMapper om = new ObjectMapper()
            .enable(SerializationFeature.INDENT_OUTPUT);

    public static void main(String[] args) throws Exception {

        Properties props = new Properties();
        props.put("bootstrap.servers", KAFKA_BROKER_URL);
        props.put("group.id", CONSUMER_GROUP);
        props.put("key.deserializer", "io.confluent.kafka.serializers.KafkaAvroDeserializer");
        props.put("value.deserializer", "io.confluent.kafka.serializers.KafkaAvroDeserializer");
        props.put("schema.registry.url", SCHEMA_REGISTRY_URL);
        props.put("specific.avro.reader", "false"); // <-- 告訴KafkaAvroDeserializer來反序列成Avro產生的generic物件類別

        props.put("auto.offset.reset", "earliest");
        props.put("enable.auto.commit", "true");


        Consumer<GenericRecord, GenericRecord> consumer = new KafkaConsumer<>(props); // msgKey是string, msgValue是"Test"

        String topicName = "dbserver1.inventory.customers";
        consumer.subscribe(Arrays.asList(topicName));

        // polling event from Kafka
        try {
            System.out.println("Start listen incoming messages ...");
            while (true) {

                ConsumerRecords<GenericRecord, GenericRecord> records = consumer.poll(Duration.ofMillis(1000));
                for (ConsumerRecord<GenericRecord, GenericRecord> record : records){

                    // Get metadata
                    String topic = record.topic();
                    int partition = record.partition();
                    long offset = record.offset();
                    TimestampType timestampType = record.timestampType();
                    long timestamp = record.timestamp();

                    GenericRecord msgKey = record.key();
                    GenericRecord msgValue = record.value();

                    // show metadata msgKey & msgValue
                    //System.out.println(topic + "-" + partition + "-" + offset + " : (" + record.key() + ", " + msgValue + ")");
                    System.out.printf("Topic: %s, Partition: %s, Offset: %s\n\n", topic, partition, offset);

                    // 以JSON的格式來把收到的訊息打印到Console上
                    printMsgKey(msgKey);
                    printMsgValue(msgValue);
                }
                consumer.commitAsync();
            }
        } finally {
            consumer.close();
            System.out.println("Stop listen incoming messages");
        }
    }

    private static void printMsgKey(GenericRecord msgKey) {
        String msgKey_json = msgKey.toString();
        try {
            System.out.println("[Message key]");
            System.out.println(om.writeValueAsString(om.readTree(msgKey_json)));
            System.out.println();
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void printMsgValue(GenericRecord msgValue) {

        try {
            System.out.println("[Message value]");
            if(msgValue==null) {
                System.out.println("NULL");
                return;
            }

            String msgValue_json = msgValue.toString();
            System.out.println(om.writeValueAsString(om.readTree(msgValue_json)));
            System.out.println();
        }catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
