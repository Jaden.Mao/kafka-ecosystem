/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ddt.das;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

/**
 * 一個發佈訊息到Kafka的範例程式。使用前請先修改：
 * (1) KAFKA_BROKER_URL
 * (2) STUDENT_ID
 */
public class BasicProducer {

    private static String KAFKA_BROKER_URL = "192.168.50.3:9092"; // 設定要連接的Kafka群
    private static String STUDENT_ID = "STUDENT_ID"; // *** <-- 修改成你/妳的學員編號

    public static void main(String[] args) {
        // 步驟1. 設定要連線到Kafka集群的相關設定
        Properties props = new Properties();
        props.put("bootstrap.servers", KAFKA_BROKER_URL); // Kafka集群在那裡?
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer"); // 指定msgKey的序列化器
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer"); // 指定msgValue的序列化器
        // 步驟2. 產生一個Kafka的Producer的實例
        Producer<String, String> producer = new KafkaProducer<>(props);
        // 步驟3. 指定想要發佈訊息的topic名稱
        String topicName = "test";
        int msgCounter = 0;
        try {
            System.out.println("Start sending messages ...");
            // 步驟4. 產生要發佈到Kafka的訊息 (把訊息封裝進一個ProducerRecord的實例中)
            //    - 參數#1: topicName
            //    - 參數#2: msgKey
            //    - 參數#3: msgValue
            producer.send(new ProducerRecord<>(topicName, null, "Hello"));
            producer.send(new ProducerRecord<>(topicName, null, "Hello2"));
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID, "Hello3"));
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID, "Hello4"));
            msgCounter+=4;
        } catch (Exception e) {
            // 錯誤處理
            e.printStackTrace();
        }
        System.out.println("Send " + msgCounter + " messages to Kafka");
        // 步驟5. 關掉Producer實例的連線
        producer.close();
        System.out.println("Message sending completed!");
    }
}
