## Deploy Apache Kafka on Kubernetes
---

### Deploy

- Start minikube
```bash
minikube start --memory=6144 --cpus=4
```
- Create related services
```bash
kubectl apply -f storageclass-hostpath.yaml
kubectl apply -f 00-namespace/
kubectl apply -f 01-zookeeper/
kubectl apply -f 02-kafka/
kubectl apply -f 03-yahoo-kafka-manager/
```
- Change namespace
```bash
kubectl config set-context --current --namespace=kafka-cluster
```

### Verify Kafka service

- Verify kafka-manager service
```bash
607 rich:minikube-kafka $ minikube service -n kafka-cluster kafka-manager --url
http://192.168.64.9:31166
```
- Enter in a kafka broker and create a topic
```bash
kafka-topics.sh --create \
  --zookeeper zookeeper-service:2181 \
  --replication-factor 1 --partitions 1 \
  --topic mytopic
```
- Create topic with 3 replica and 3 partition
```bash
kafka-topics.sh --create \
  --zookeeper zookeeper-service:2181 \
  --replication-factor 3 --partitions 3 \
  --topic test
```
- Produce & consume record
```bash
kafka-console-producer.sh --broker-list localhost:9092 --topic test

kafka-console-consumer.sh --bootstrap-server localhost:9092 \
  --topic test --from-beginning
```

- Delete all service
```bash
kubectl delete --all statefulset  --namespace=kafka-cluster
kubectl delete --all service  --namespace=kafka-cluster
kubectl delete --all deploy  --namespace=kafka-cluster
```