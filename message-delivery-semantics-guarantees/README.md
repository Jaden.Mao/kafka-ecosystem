## Message Delivery Semantics and Guarantees

### 3 Semantics and Guarantees
- **At-most-once**: This means that a message will never be delivered more than once but messages might be lost.
- **At-least-once**: This means that we'll never lose a message but a message might end up being delivered to a consumer more than once.
- **Exactly-once**: All messages will be delivered exactly one time and is the most desirable guarantee, but also a poorly understood one.

### Exactly-once semantics in Apache Kafka
Prior to 0.11.x, Apache Kafka supported at-least once delivery semantics and in-order delivery per partition.

- Idempotence: Exactly-once in order semantics per partition
  - configure your producer `enable.idempotence=true`
     - each batch of messages sent to Kafka will contain a sequence number which the broker will use to dedupe any duplicate send
     - this sequence number is persisted to the replicated log, so even if the leader fails, any broker that takes over will also know if a resend is a duplicate
```java
Properties props = new Properties();
props.put("bootstrap.servers", "localhost:9092");
props.put("enable.idempotence", "true");
// Kafka will use this transaction id as part of its algorithm to deduplicate any message this producer sends, ensuring idempotency
props.put("transactional.id", "producer-1");

```

- Transactions: Atomic writes across multiple partitions
  - Kafka now supports atomic writes across multiple partitions through the new [transactions API](http://kafka.apache.org/20/javadoc/org/apache/kafka/clients/producer/Producer.html)
  - This allows a producer to send a batch of messages to multiple partitions such that either all messages in the batch are eventually visible to any consumer or none are ever visible to consumers.
  - It is worth noting that a Kafka topic partition might have some messages that are part of a transaction while others that are not.
  - So on the Consumer side, you have two options for reading transactional messages, expressed through the `isolation.level` consumer config:
    - read_committed: In addition to reading messages that are not part of a transaction, also be able to read ones that are, after the transaction is committed.
    - read_uncommitted: Read all messages in offset order without waiting for transactions to be committed. This option is similar to the current semantics of a Kafka consumer.

```java
producer.initTransactions();
try {
  producer.beginTransaction();
  producer.send(record1);
  producer.send(record2);
  producer.commitTransaction();
} catch(ProducerFencedException e) {
  producer.close();
} catch(KafkaException e) {
  producer.abortTransaction();
}
```

### [Transactions in Kafka](https://www.confluent.io/blog/transactions-apache-kafka/)
- [Spring CLoud Stram Transactional Binder](https://cloud.spring.io/spring-cloud-static/spring-cloud-stream-binder-kafka/3.0.0.M3/reference/html/spring-cloud-stream-binder-kafka.html#kafka-transactional-binder)

### Reference
- https://www.confluent.io/blog/exactly-once-semantics-are-possible-heres-how-apache-kafka-does-it/
- https://www.baeldung.com/kafka-exactly-once
- https://jack-vanlightly.com/blog/2017/12/15/rabbitmq-vs-kafka-part-4-message-delivery-semantics-and-guarantees
- https://supergloo.com/kafka/kafka-architecture-delivery/
- https://hevodata.com/blog/kafka-exactly-once/