## Kafka Producer API

### 3 methods of sending messages

#### [Kafka Java SDK](https://kafka.apache.org/23/javadoc/index.html?org/apache/kafka/clients/producer/KafkaProducer.html)

- Fire-and-forget: 
We send a message to the server and don’t really care if it arrives succesfully or not.
```java
Producer<String, String> producer = new KafkaProducer<>(props);
ProducerRecord<String, String> record = new ProducerRecord<>("CustomerCountry", "Precision Products", "France");
producer.send(record);
```

- Synchronous:
We send a message, the send() method returns a Future object, and we use get() to wait on the future and see if the send() was successful or not.

```java
try {
  producer.send(record).get();  //This method
will throw an exception if the record is not sent successfully to Kafka.
} catch (Exception e) {
  //log and encounter an exception or retry
}
```
  - Asynchronous:
We call the send() method with a callback function, which gets triggered when it receives a response from the Kafka broker.

```java
private class ProducerCallback implements Callback {
    @Override
    public void onCompletion(RecordMetadata recordMetadata, Exception e) {
        if (e != null) {
            log.error("Data sending failed!", e)
            //retry
        }
    }
}

producer.send(record, new ProducerCallback());
```

### [Spring Cloud Stream Kafka Producer Properties](https://docs.spring.io/spring-cloud-stream/docs/current/reference/htmlsingle/#kafka-producer-properties)

```yml
#  Whether the producer is synchronous. (Default: false)
spring.cloud.stream.kafka.bindings.<channelName>.producer.sync: true
```

