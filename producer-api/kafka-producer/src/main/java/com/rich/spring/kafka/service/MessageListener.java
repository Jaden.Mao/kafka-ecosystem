package com.rich.spring.kafka.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class MessageListener {

    List<String> recordStorage = new ArrayList<>();

    @KafkaListener(topics = MessageSender.topic, groupId = "group1")
    private void listen(String message) {
        log.info("Received message: {}", message);
        recordStorage.add(message);
    }

    public List<String> getAllRecords() {
        return recordStorage;
    }

}
