package com.rich.spring.kafka.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MessageSender {

    public static final String topic = "messages";

    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Autowired
    KafkaTemplate kafkaTemplate;

    public void send(String message) {
        log.info("send message: {} to topic: {}", message, topic);
        kafkaTemplate.send(topic, message);
    }
}
