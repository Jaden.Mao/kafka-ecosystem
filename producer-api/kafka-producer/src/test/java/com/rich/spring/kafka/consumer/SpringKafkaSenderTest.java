package com.rich.spring.kafka.consumer;

import com.rich.spring.kafka.service.MessageSender;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.rule.EmbeddedKafkaRule;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
public class SpringKafkaSenderTest {

  @Autowired
  private MessageSender sender;

  @ClassRule
  public static EmbeddedKafkaRule embeddedKafka =
      new EmbeddedKafkaRule(1, true, MessageSender.topic);

  @Before
  public void setUp() throws Exception {

  }

  @After
  public void tearDown() {
  }

  @Test
  public void testSend() {
    String greeting = "Hello Spring Kafka!";
    sender.send(greeting);
  }
}
